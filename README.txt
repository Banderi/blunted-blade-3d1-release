*** Blunted Blade pre-alpha 0.10 ***


Blunted Blade(TM) is a in-development indie game featuring sandbox, RPG and First Person
Shooter mechanics. Blunted Blade(TM) is currently under development by Banderi.


=== DISCLAIMER ===
This is a testing build only. No assets, mechanics or code snippets, in part or whole,
are to be considered representative of the final pruduct.
Many disfunctional and bugged features are currently in the demo that will be fixed with
future releases. Please let us know if you encounter any issue!


=== CONTROLS ===

Use W, A, S, D to control the character, the mouse to move the camera around and M1, M2 and
M3 (mouse wheel button) to shoot. (controls are W.I.P.)
Press E to open the inventory menu, ESC to return to the game.
Press ESC to access the pause/options menu.
While in the crafting menu: use LEFT and RIGHT to rotate the weapon part, UP and DOWN to
change the current part. Use Left CONTROL and Right CONTROL to change the attachment slot.


=== CHANGELOG ===

= 0.10:
  - fixed weapon building menu bugs
  - added repository for release binary builds
  - fixed character movement
= 0.9:
  - merged ingame RPG mechanics and weapon-crafting menu
= 0.8:
  - added inventory mechanics
  - added ingame character movement
